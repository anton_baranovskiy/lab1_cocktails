﻿namespace AnthonyCocktail
{
	partial class Barman
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.buttonReload = new System.Windows.Forms.Button();
			this.dataGridViewCocktails = new System.Windows.Forms.DataGridView();
			this.recipesBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.dB_MainDataSetRecipes = new AnthonyCocktail.DB_MainDataSetRecipes();
			this.recipesTableAdapter = new AnthonyCocktail.DB_MainDataSetRecipesTableAdapters.RecipesTableAdapter();
			this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.editRecipe = new System.Windows.Forms.DataGridViewButtonColumn();
			this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.recipeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabControl1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewCocktails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.recipesBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dB_MainDataSetRecipes)).BeginInit();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(13, 39);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(767, 393);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(737, 367);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Обработка заказов";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.dataGridViewCocktails);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(759, 367);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Редактор коктейлей";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// buttonReload
			// 
			this.buttonReload.Location = new System.Drawing.Point(546, 6);
			this.buttonReload.Name = "buttonReload";
			this.buttonReload.Size = new System.Drawing.Size(208, 27);
			this.buttonReload.TabIndex = 1;
			this.buttonReload.Text = "Обновить с добавлением";
			this.buttonReload.UseVisualStyleBackColor = true;
			this.buttonReload.Click += new System.EventHandler(this.buttonSync_Click);
			// 
			// dataGridViewCocktails
			// 
			this.dataGridViewCocktails.AutoGenerateColumns = false;
			this.dataGridViewCocktails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewCocktails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.editRecipe,
            this.priceDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn,
            this.recipeDataGridViewTextBoxColumn});
			this.dataGridViewCocktails.DataSource = this.recipesBindingSource;
			this.dataGridViewCocktails.Location = new System.Drawing.Point(6, 6);
			this.dataGridViewCocktails.MultiSelect = false;
			this.dataGridViewCocktails.Name = "dataGridViewCocktails";
			this.dataGridViewCocktails.Size = new System.Drawing.Size(457, 381);
			this.dataGridViewCocktails.TabIndex = 0;
			this.dataGridViewCocktails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCocktails_CellClick);
			// 
			// recipesBindingSource
			// 
			this.recipesBindingSource.DataMember = "Recipes";
			this.recipesBindingSource.DataSource = this.dB_MainDataSetRecipes;
			// 
			// dB_MainDataSetRecipes
			// 
			this.dB_MainDataSetRecipes.DataSetName = "DB_MainDataSetRecipes";
			this.dB_MainDataSetRecipes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// recipesTableAdapter
			// 
			this.recipesTableAdapter.ClearBeforeFill = true;
			// 
			// nameDataGridViewTextBoxColumn
			// 
			this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
			this.nameDataGridViewTextBoxColumn.HeaderText = "Название";
			this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
			this.nameDataGridViewTextBoxColumn.Width = 200;
			// 
			// editRecipe
			// 
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.NullValue = "Изменить";
			this.editRecipe.DefaultCellStyle = dataGridViewCellStyle2;
			this.editRecipe.HeaderText = "Рецепт";
			this.editRecipe.Name = "editRecipe";
			this.editRecipe.ReadOnly = true;
			this.editRecipe.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.editRecipe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.editRecipe.Text = "Изменить";
			this.editRecipe.ToolTipText = "Изменить рецепт";
			// 
			// priceDataGridViewTextBoxColumn
			// 
			this.priceDataGridViewTextBoxColumn.DataPropertyName = "price";
			this.priceDataGridViewTextBoxColumn.HeaderText = "Цена";
			this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
			// 
			// idDataGridViewTextBoxColumn
			// 
			this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
			this.idDataGridViewTextBoxColumn.HeaderText = "Id";
			this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
			this.idDataGridViewTextBoxColumn.ReadOnly = true;
			this.idDataGridViewTextBoxColumn.Visible = false;
			// 
			// recipeDataGridViewTextBoxColumn
			// 
			this.recipeDataGridViewTextBoxColumn.DataPropertyName = "recipe";
			this.recipeDataGridViewTextBoxColumn.HeaderText = "recipeText";
			this.recipeDataGridViewTextBoxColumn.Name = "recipeDataGridViewTextBoxColumn";
			this.recipeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.recipeDataGridViewTextBoxColumn.Visible = false;
			// 
			// Barman
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(792, 444);
			this.Controls.Add(this.buttonReload);
			this.Controls.Add(this.tabControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "Barman";
			this.Text = "Бармен";
			this.Load += new System.EventHandler(this.BarmanForm_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewCocktails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.recipesBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dB_MainDataSetRecipes)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.DataGridView dataGridViewCocktails;
		private DB_MainDataSetRecipes dB_MainDataSetRecipes;
		private System.Windows.Forms.BindingSource recipesBindingSource;
		private DB_MainDataSetRecipesTableAdapters.RecipesTableAdapter recipesTableAdapter;
		private System.Windows.Forms.Button buttonReload;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewButtonColumn editRecipe;
		private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn recipeDataGridViewTextBoxColumn;
	}
}