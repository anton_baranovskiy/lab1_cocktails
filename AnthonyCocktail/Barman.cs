﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnthonyCocktail
{
	public partial class Barman : Form
	{
		public Barman()
		{
			InitializeComponent();
		}

		private void BarmanForm_Load(object sender, EventArgs e)
		{
			// TODO: данная строка кода позволяет загрузить данные в таблицу "dB_MainDataSetRecipes.Recipes". При необходимости она может быть перемещена или удалена.
			this.recipesTableAdapter.Fill(this.dB_MainDataSetRecipes.Recipes);
		}

		private void dataGridViewCocktails_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == editRecipe.Index)
			{
				EditRecipe editRecipeF = new EditRecipe(
						dataGridViewCocktails[recipeDataGridViewTextBoxColumn.Name, e.RowIndex].Value.ToString()
					);
				editRecipeF.ShowDialog();
				if (editRecipeF.DialogResult == System.Windows.Forms.DialogResult.OK)
				{
					dataGridViewCocktails[recipeDataGridViewTextBoxColumn.Name, e.RowIndex].Value =
						editRecipeF.GetResult();
				}
			}
		}

		private void buttonSync_Click(object sender, EventArgs e)
		{
			try
			{
				this.Validate();

				this.recipesBindingSource.EndEdit();
				this.recipesTableAdapter.Update(this.dB_MainDataSetRecipes);

				// TODO: данная строка кода позволяет загрузить данные в таблицу "dB_MainDataSetRecipes.Recipes". При необходимости она может быть перемещена или удалена.
				this.recipesTableAdapter.Fill(this.dB_MainDataSetRecipes.Recipes);
			}
			catch (System.Exception ex)
			{
				MessageBox.Show("Error!");
			}
		}
	}
}
