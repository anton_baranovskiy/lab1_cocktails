﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnthonyCocktail
{
	public partial class EditRecipe : Form
	{

		string result = string.Empty;
		string recipeText = string.Empty;

		public EditRecipe(string recipeText)
		{
			InitializeComponent();
			this.recipeText = recipeText;
		}

		private void EditRecipeForm_Load(object sender, EventArgs e)
		{
			// TODO: данная строка кода позволяет загрузить данные в таблицу "dB_MainDataSetTare.Tare". При необходимости она может быть перемещена или удалена.
			this.tareTableAdapter.Fill(this.dB_MainDataSetTare.Tare);
			// TODO: данная строка кода позволяет загрузить данные в таблицу "dB_MainDataSetProduct.Products". При необходимости она может быть перемещена или удалена.
			this.productsTableAdapter.Fill(this.dB_MainDataSetProduct.Products);
			if (recipeText != String.Empty)
			{
				string[] mas = recipeText.Split(new char[] { '&' });
				foreach (string product in mas[0].Split(new char[] { ';' }))
				{
					string[] info = product.Split(new char[] { ':' });
					int counter = 0;
					foreach (DataGridViewRow row in dataGridViewProducts.Rows)
					{
						if ((int)row.Cells[idDataGridViewTextBoxColumn.Name].Value == int.Parse(info[0]))
						{
							dataGridViewProducts.Rows[counter].Cells[quantityDataGridViewTextBoxColumn.Name].Value =
								decimal.Parse(info[1]);
						}
						counter++;
					}
				}
				foreach (string tare in mas[1].Split(new char[] { ';' }))
				{
					string[] info = tare.Split(new char[] { ':' });
					int counter = 0;
					foreach (DataGridViewRow row in dataGridViewTare.Rows)
					{
						if ((int)row.Cells[idDataGridViewTextBoxColumn1.Name].Value == int.Parse(info[0]))
						{
							dataGridViewTare.Rows[counter].Cells[quamtityDataGridViewTextBoxColumn.Name].Value =
								decimal.Parse(info[1]);
						}
						counter++;
					}
				}
			}
		}

		public string GetResult()
		{
			return result;
		}

		private void buttonOk_Click(object sender, EventArgs e)
		{
			bool normal = false;
			bool okData = true;
			foreach (DataGridViewRow row in dataGridViewProducts.Rows)
			{
				if (row.Cells[quantityDataGridViewTextBoxColumn.Name].Value != null)
				{
					result += row.Cells[idDataGridViewTextBoxColumn.Name].Value.ToString()
						+ ":" + row.Cells[quantityDataGridViewTextBoxColumn.Name].Value.ToString() + ";";
					normal = true;
				}
			}
			if (normal == false)
			{
				MessageBox.Show("Не один продук не выбран!");
				okData = false;
			}
			else
			{
				normal = false;
				result = result.Substring(0, result.Length - 1);
				result += "&";
				foreach (DataGridViewRow row in dataGridViewTare.Rows)
				{
					if (row.Cells[quamtityDataGridViewTextBoxColumn.Name].Value != null)
					{
						result += row.Cells[idDataGridViewTextBoxColumn1.Name].Value.ToString()
						+ ":" + row.Cells[quamtityDataGridViewTextBoxColumn.Name].Value.ToString() + ";";
						normal = true;
					}
				}
				if (normal == false)
				{
					MessageBox.Show("Не одна тара не выбрана!");
					okData = false;
				}
				else
				{
					result = result.Substring(0, result.Length - 1);
					if (okData)
					{
						DialogResult = System.Windows.Forms.DialogResult.OK;
					}
				}
			}
		}
	}
}
