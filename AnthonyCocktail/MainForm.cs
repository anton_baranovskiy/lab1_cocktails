﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnthonyCocktail
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (radioButton1.Checked == true)
			{
				Barman barman = new Barman();
				barman.ShowDialog();
			}
			else
			{
				Admin admin = new Admin();
				admin.ShowDialog();
			}
		}
	}
}
